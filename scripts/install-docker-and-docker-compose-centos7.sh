#!/bin/bash
yum -y update
yum groupinstall -y "Development Tools"
yum install -y yum-utils device-mapper-persistent-data lvm2 python3
yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
yum install -y docker-ce
usermod -aG docker vagrant
systemctl enable docker.service
systemctl start docker.service
curl "https://bootstrap.pypa.io/get-pip.py" -o "get-pip.py"
python3 get-pip.py
/usr/local/bin/pip3 install docker-compose
yum upgrade -y python*
/usr/local/bin/docker-compose version