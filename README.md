# Prometheus & Grafana

Ejemplo de Docker Compose para Prometheus & Grafana

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.


### Prerequisites

Versiones recientes de docker, docker-compose

### Deployment

Una vez clonado el proyecto se debe ejecutar el docker compose

```bash
$ docker-compose up -d
```
Para verificar si todo está funcionando

```bash
$ docker-compose ps
```
### Finalizar

Para detener el proyecto

```bash
$ docker-compose stop
```

Para detener el proyecto y REMOVER los contenedores detenidos así como todas las redes creadas

```bash
$ docker-compose down
```

Para detener el proyecto y REMOVER los contenedores detenidos así como todas las redes creadas y eliminar los volumenes 

```bash
$ docker-compose down -v
```

## Contributing

Please read [CONTRIBUTING.md](https://gist.github.com/PurpleBooth/b24679402957c63ec426) for details on our code of conduct, and the process for submitting pull requests to us.

## Authors

* **Mauro Mejia Vargas** - *Initial work* - [Mauro Mejia](https://gitlab.com/jmmejiav/)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* **Audel Diaz** - *Enfoque de Vagrant* - [Audel Diaz](https://gitlab.com/AudelDiaz)

* Colombian Coffee

